import express from 'express';
import cors from 'cors';
import bodyParser from 'body-parser';
import dotenv from 'dotenv';
import multer from 'multer';
import { v4 as uuidv4 } from 'uuid';
import { JsonDB } from 'node-json-db';
import { Config } from 'node-json-db/dist/lib/JsonDBConfig';

const STORAGE_CONFIG = multer.diskStorage({
    destination: (req, file, cb) => {
        cb(null, './server/uploads');
    },
    filename: (req, file, cb) => {
        cb(null, Date.now() + '--' + file.originalname);
    }
});

const fileFilter = (req, file, cb) => {
    cb(null, file.mimetype.search('image') !== -1);
};

const APP = express();
const UPLOAD = multer({ storage: STORAGE_CONFIG, fileFilter: fileFilter });

/**
 * =========== APP CONFIG ===========
 */
APP.use(cors());
dotenv.config();

// parse application/x-www-form-urlencoded
APP.use(bodyParser.urlencoded({ extended: false }))

// parse application/json
APP.use(bodyParser.json())

APP.use('/static', express.static('server/uploads'));

const ENV = process.env;
const PORT = ENV.SERVER_PORT;

/**
 * =========== /// APP CONFIG ===========
 */

/**
 * =========== DB CONFIG ===========
 */
const DB = new JsonDB(new Config(ENV.DB_NAME, true, false, '/'));

/**
 * =========== METHODS =========== 
 */
const getProducts = id => {
    let products = [];
    
    try {
        products = DB.getData(ENV.PRODUCTS_URL);
    } catch(err) {
        DB.push(ENV.PRODUCTS_URL, products);
    }

    if (id) {
        return products.find(p => p.id === id) || null;
    }

    return products;
};
/**
 * =========== /// METHODS =========== 
 */

/**
 * =========== ROUTER =========== 
 */

// GET all products
APP.get(ENV.PRODUCTS_URL, (req, res) => {
    res.json(getProducts());
});

// GET target product
APP.get(`${ENV.PRODUCTS_URL}/:id`, (req, res) => {
    res.json(getProducts(req.params.id));
});

// POST new product
APP.post(ENV.PRODUCTS_URL, UPLOAD.single('product_image'), (req, res) => {
    const {
        title='default title',
        description=null,
        price=0
    } = req.body;

    let file = null;

    if (req.file && req.file.mimetype.search('image') !== -1) {
        file = `${ENV.SERVER_URL}:${PORT}/static/${req.file.filename}`;
    }

    const newProduct = {
        id: uuidv4(),
        title,
        description,
        price,
        image: file
    };

    DB.push(ENV.PRODUCTS_URL, [...getProducts(), newProduct]);

    res.send(newProduct);
});

// DELETE product
APP.delete(`${ENV.PRODUCTS_URL}/:id`, (req, res) => {
    const products = getProducts();
    const targetProduct = products.find(p => p.id === req.params.id);
    
    if (targetProduct) {
        DB.push(ENV.PRODUCTS_URL, products.filter(p => p.id !== req.params.id));
        res.send(`Product was deleted - "${targetProduct.title}"`);
    } else {
        res.send('Product does not exist');
    }

});

// UPDATE product
APP.put(`${ENV.PRODUCTS_URL}/:id`, UPLOAD.single('product_image'), (req, res) => {
    const products = getProducts();
    const targetProduct = products.find(p => p.id === req.params.id);

    if (targetProduct) {

        for (let i = 0; i < products.length; i++) {
            const current = products[i];

            if (current === targetProduct) {
                
                products[i] = {
                    ...current,
                    ...req.body,
                    image: req.file ? `${ENV.SERVER_URL}:${PORT}/static/${req.file.filename}` : null
                }

                break;
            }
        }

        DB.push(ENV.PRODUCTS_URL, products);
        res.send(`Product was updated - "${targetProduct.title}"`);
    } else {
        res.send('Product does not exist');
    }
});

/**
 * =========== /// ROUTER =========== 
 */

/**
 * =========== SERVER INIT ===========
 */
APP.listen(PORT, () => {
    console.log('Server was running on ' + PORT);
});
<p align="center"><img src="https://artpix3d.com/wp-content/uploads/2017/10/cropped-21369469_2378190085738469_2191064133421690870_n-192x192.png" width="200"></p>

# Artpix test project

**1. Instalation**
- npm install

**2. Run**
- npm start

## Overview
Develop products' list

## Functionals
- Create
- Delete
- Edit
- Show products' list

You can mix Create/Edit page

## DON'T USE
- Storage libraries (Redux, MOBx...)
- Create-react-app
- Server folder ( Readonly )

## Requirements
- React
- Storage as (context + reducer) by hooks
- Styled components
- Use ENV variables
- Comments
- Your branch from master with name 'AP-YourName-YourSname'

## Finally
- Commit your changes
- Push into your branch
- Create merge request to master

## API
You have a simple express server with CRUD

- URL - env.PRODUCTS_ENDPOINT

- GET:URL - Get products' list
- GET:URL\id - Get target product
- POST:URL - Add new product
- DELETE:URL\id - Delete product
- PUT:URL\id - update product

## Request data
- title - String
- price - Number
- description - String
- product_image - image file
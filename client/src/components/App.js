import React from 'react';
import {
    Wrapper,
    Image,
    Title,
    Subtitle,
    Accent
} from './Styled';
import Logo from '../assets/images/logo.png';

const App = () => {
    return (
        <Wrapper>
            <Image src={Logo} alt="logo" />
            <Title>Hello Newcomer!</Title>
            <Subtitle>
                <Accent>This is your test boilerplate</Accent> <br />
                Good luck!
            </Subtitle>
        </Wrapper>
    );
};

export default App;
 
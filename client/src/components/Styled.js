import styled from "styled-components";
import BG from '../assets/images/bg.jpg';
import RockImage from '../assets/images/rock.png';

export const Wrapper = styled.div`
    display: flex;
    align-items: center;
    justify-content: center;
    flex-direction: column;
    height: 100vh;
    background: #1E2327 url(${BG}) no-repeat;
    background-position: top right;
    color: #fff;
    text-align: center;
    position: relative;

    &:after {
        content: '';
        display: block;
        position: absolute;
        bottom: 0;
        left: 50%;
        transform: translateX(-50%);
        background: url(${RockImage}) no-repeat;
        height: 271px;
        width: 191px;
        opacity: 0.1;
        margin-left: -25px;
    }

    @media (max-width: 1200px) {
        background-position: top center;
    }
`;

export const Image = styled.img`
    margin-bottom: 40px;
    display: inline-block;
    max-width: 160px;
`;

export const Title = styled.h1`
    margin-bottom: 8px;
    font-family: Lato;
    font-weight: bold;
    font-size: 24px;
    line-height: 28px;
`;

export const Subtitle = styled.p`
    font-size: 14px;
    line-height: 30px;
    font-weight: 400;
`;

export const Accent = styled.span`
    font-size: 12px;
    color: #1F73B7;
`;
const path = require('path');
const HtmlWebPackPlugin = require("html-webpack-plugin");
const Dotenv = require("dotenv-webpack");

/**
 *  Plugin for read HTML
 */
const htmlPlugin = new HtmlWebPackPlugin({
    template: path.resolve(__dirname, './client/public', 'index.html'),
    filename: 'index.html'
});


const runClient = () => ({
    entry: ["@babel/polyfill", "./client/src/index.js"],
    output: {
        filename: 'bundle.js',
        path: path.resolve(__dirname, 'dist'),
        publicPath: '/',
    },
    devServer: {
        historyApiFallback: true
    },
    module: {
        rules: [
            {
                test: /\.js$/,
                exclude: /node_modules/,
                use: 'babel-loader',
            },
            {
                test: /\.css$/,
                exclude: /node_modules/,
                use: ['style-loader', 'css-loader'],
            },
            {
                test: /\.sass$/,
                exclude: /node_modules/,
                use: ['style-loader', 'css-loader', 'sass-loader'],
            },
            {
                test: /\.(png|jpe?g|svg|gif)?$/,
                exclude: /node_modules/,
                loader: 'file-loader',
                options: {
                    outputPath: 'images/'
                }
            }
        ]
    },
    plugins: [
        htmlPlugin,
        new Dotenv()
    ],
    devtool: 'source-map'
});

module.exports = [
    runClient()
]
